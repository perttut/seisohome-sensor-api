// tslint:disable-next-line:no-implicit-dependencies
import { APIGatewayProxyResult } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { ScanInput } from 'aws-sdk/clients/dynamodb';
import { isNullOrUndefined } from 'util';
import { lambdaReturnValue } from './util/lambda-util';
import { getBooleanValue, getStringValue, getUnitValue, SENSOR_CONFIG_TABLE, SensorConfig } from './util/sensor-common';

// tslint:disable-next-line:no-unused-variable
export const handler = async (): Promise<APIGatewayProxyResult> => {
  // TODO: move sensor config reading to common repository
  const dynamoDb = new DynamoDB({apiVersion: '2012-08-10'});
  const scanInput: ScanInput = {TableName: SENSOR_CONFIG_TABLE};
  const sensorConfigItems = await dynamoDb
    .scan(scanInput)
    .promise();
  if (isNullOrUndefined(sensorConfigItems) || isNullOrUndefined(sensorConfigItems.Items)) {
    console.error('DynamoDB scan returned nothing for configuration');

    return lambdaReturnValue(500, 'DynamoDB scan returned nothing for configuration');
  }

  try {
    const sensorConfigs: Array<SensorConfig> = sensorConfigItems.Items.map(item => new SensorConfig(
      getStringValue('id', item),
      getStringValue('name', item),
      getStringValue('hardwareId', item),
      getUnitValue('unit', item),
      getBooleanValue('enabled', item)));

    return lambdaReturnValue(200, sensorConfigs);
  } catch (error) {
    const errorMsg = 'Unable to read sensor configuration from DynamoDB';
    console.error(errorMsg, error);

    return lambdaReturnValue(500, `${errorMsg}: ${error}`);
  }
};
