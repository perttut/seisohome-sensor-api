// tslint:disable-next-line:no-implicit-dependencies
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { PutItemInput } from 'aws-sdk/clients/dynamodb';
import { isNullOrUndefined } from 'util';
import { lambdaReturnValue } from './util/lambda-util';
import { SENSOR_CONFIG_TABLE, SensorConfig } from './util/sensor-common';

const sendSensorConfigs = async (sensorConfigs: Array<SensorConfig>): Promise<void> => {
  const dynamoDb = new DynamoDB({apiVersion: '2012-08-10'});
  const createPutItem = (sensorConfig: SensorConfig): PutItemInput => ({
    TableName: SENSOR_CONFIG_TABLE,
    Item: {
      id: {S: sensorConfig.id},
      name: {S: sensorConfig.name},
      hardwareId: {S: sensorConfig.hardwareId},
      unit: {S: sensorConfig.unit},
      enabled: {BOOL: sensorConfig.enabled}
    }
  });
  const putItems: Array<PutItemInput> = sensorConfigs.map(createPutItem);
  const putPromises = putItems.map(putItem => dynamoDb
    .putItem(putItem)
    .promise());
  await Promise.all(putPromises);
};

// tslint:disable-next-line:no-unused-variable
export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  try {
    if (isNullOrUndefined(event.body)) {
      return lambdaReturnValue(500, 'Request body is missing');
    }
    // TODO: add input validation
    const sensorConfigs = JSON.parse(event.body);
    await sendSensorConfigs(sensorConfigs);

    return lambdaReturnValue(200, 'OK');
  } catch (error) {
    const errorMsg = 'Unable to store sensor configuration to DynamoDB';
    console.error(errorMsg, error);

    return lambdaReturnValue(500, `${errorMsg}: ${error}`);
  }
};
