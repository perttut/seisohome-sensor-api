// tslint:disable-next-line:no-implicit-dependencies
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { QueryInput } from 'aws-sdk/clients/dynamodb';
import { isNullOrUndefined } from 'util';
import { lambdaReturnValue } from './util/lambda-util';
import {
  getNumberValue,
  getStringValue,
  SENSOR_DATA_TABLE,
  SensorData
} from './util/sensor-common';

const queryLatest = async (sensorId: string): Promise<SensorData | undefined> => {
  const dynamoDb = new DynamoDB({apiVersion: '2012-08-10'});

  const fiveMinutes = 5 * 60 * 1000;
  const currentTime: number = Date.now();

  const queryInput: QueryInput = {
    TableName: SENSOR_DATA_TABLE,
    KeyConditionExpression: '#sid = :sensorId and #ts between :fiveMinsAgo and :now',
    ExpressionAttributeNames: {
      '#sid': 'id',
      '#ts': 'timestamp'
    },
    ExpressionAttributeValues: {
      ':sensorId': {S: sensorId},
      ':now': {N: currentTime.toString()},
      ':fiveMinsAgo': {N: (currentTime - fiveMinutes).toString()}
    },
    // we're interested only in the latest
    Limit: 1,
    // greatest timestamp first
    ScanIndexForward: false
  };
  const sensorDataItems = await dynamoDb
    .query(queryInput)
    .promise();

  if (isNullOrUndefined(sensorDataItems) || isNullOrUndefined(sensorDataItems.Items)) {
    const errorMsg = 'DynamoDB query returned nothing for sensor data';
    console.error(errorMsg);
    throw new Error(errorMsg);
  }
  if (sensorDataItems.Items.length > 1) {
    const errorMsg = `Unexpected number of query results: ${sensorDataItems.Items.length}`;
    console.error(errorMsg);
    throw new Error(errorMsg);
  }
  if (sensorDataItems.Items.length === 1) {
    const sensorDataItem = sensorDataItems.Items[0];

    return new SensorData(
      getStringValue('id', sensorDataItem),
      getNumberValue('timestamp', sensorDataItem),
      getNumberValue('value', sensorDataItem));
  }

  return undefined;
};

// tslint:disable-next-line:no-unused-variable
export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  if (isNullOrUndefined(event.pathParameters) || isNullOrUndefined(event.pathParameters.id)) {
    return lambdaReturnValue(500, 'Path parameter id missing');
  }
  const sensorId = event.pathParameters.id;

  try {
    const sensorData = await queryLatest(sensorId);

    return lambdaReturnValue(200, sensorData);
  } catch (error) {
    const errorMsg = `Unable to read latest ${sensorId} sensor data from DynamoDB`;
    console.error(errorMsg, error);

    return lambdaReturnValue(500, `${errorMsg}: ${error}`);
  }
};
