// tslint:disable-next-line:no-implicit-dependencies
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { QueryInput } from 'aws-sdk/clients/dynamodb';
import { isNullOrUndefined } from 'util';
import { lambdaReturnValue } from './util/lambda-util';
import {
  getNumberValue,
  getStringValue,
  SENSOR_DATA_TABLE,
  SensorData
} from './util/sensor-common';

const query = async (sensorId: string, startTime: number, endTime: number): Promise<Array<SensorData>> => {
  const dynamoDb = new DynamoDB({apiVersion: '2012-08-10'});

  const queryInput: QueryInput = {
    TableName: SENSOR_DATA_TABLE,
    KeyConditionExpression: '#sid = :sensorId and #ts between :startTime and :endTime',
    ExpressionAttributeNames: {
      '#sid': 'id',
      '#ts': 'timestamp'
    },
    ExpressionAttributeValues: {
      ':sensorId': {S: sensorId},
      ':startTime': {N: startTime.toString()},
      ':endTime': {N: endTime.toString()}
    }
  };
  const sensorDataItems = await dynamoDb
    .query(queryInput)
    .promise();

  if (isNullOrUndefined(sensorDataItems) || isNullOrUndefined(sensorDataItems.Items)) {
    const errorMsg = 'DynamoDB query returned nothing for sensor data';
    console.error(errorMsg);
    throw new Error(errorMsg);
  }

  return sensorDataItems.Items.map(sensorDataItem => new SensorData(
    getStringValue('id', sensorDataItem),
    getNumberValue('timestamp', sensorDataItem),
    getNumberValue('value', sensorDataItem)));
};

// tslint:disable-next-line:no-unused-variable
export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  const maxQueryTime = 60 * 60 * 24 * 1000; // 24h
  if (isNullOrUndefined(event.pathParameters) || isNullOrUndefined(event.pathParameters.id)) {
    return lambdaReturnValue(500, 'Path parameter id missing');
  }
  const sensorId = event.pathParameters.id;
  if (isNullOrUndefined(event.body)) {
    return lambdaReturnValue(500, 'Request body is missing');
  }
  const body = JSON.parse(event.body);
  const startTimeStr = body.startTime;
  const endTimeStr = body.endTime;
  if (isNullOrUndefined(startTimeStr) || isNullOrUndefined(endTimeStr)) {
    return lambdaReturnValue(500, 'Expected startTime and endTime in the request body');
  }
  const startTime = Number.parseInt(startTimeStr, 10);
  const endTime = Number.parseInt(endTimeStr, 10);
  if (startTime > endTime) {
    return lambdaReturnValue(500, 'startTime > endTime');
  }
  if (endTime - startTime > maxQueryTime) {
    return lambdaReturnValue(500, `Too long query period, max period is ${maxQueryTime}`);
  }

  try {
    const sensorData = await query(sensorId, startTime, endTime);

    return lambdaReturnValue(200, sensorData);
  } catch (error) {
    const errorMsg = `Unable to query periodical sensor data for ${sensorId} from DynamoDB`;
    console.error(errorMsg, error);

    return lambdaReturnValue(500, `${errorMsg}: ${error}`);
  }
};
