// tslint:disable-next-line:no-implicit-dependencies
import { APIGatewayProxyResult } from 'aws-lambda';

/**
 * Create lambda response value understood by API Gateway.
 *
 * @param statusCode  HTTP status code returned by API Gateway.
 * @param body        Object returned as response body.
 */
export const lambdaReturnValue = (statusCode: number, body: any): APIGatewayProxyResult => ({
  statusCode,
  body: JSON.stringify(body),
  headers: {'Access-Control-Allow-Origin': '*'}
});
