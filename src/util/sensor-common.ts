/**
 * TODO: MOST OF THIS FILE (IF NOT ALL), SHOULD BE MOVED TO COMMON CODE REPOSITORY TO BE SHARED WITH seisohome-ruuvitag
 */

export const SENSOR_CONFIG_TABLE = 'sensor-config';
export const SENSOR_DATA_TABLE = 'sensor';

/**
 * Units used in the system.
 * TODO: move units to common repository later
 */
import { AttributeMap } from 'aws-sdk/clients/dynamodb';
import { isNullOrUndefined } from 'util';

export enum Unit {
  Celcius = 'C',
  Pascal = 'Pa',
  Humidity = 'H%',
  // TODO: acceleration should be relative to axis (X,Y,Z). Or each axis should be independent "sensor"
  // Acceleration = 'mG',
  Voltage = 'V',
  KWh = 'kWh'
}

// TODO: move SensorConfig class to common repository
export class SensorConfig {
  /**
   * @param id          User assigned id of the sensor. Unique in the system.
   * @param name        Human readable name of the sensor
   * @param hardwareId  The hardware id of the sensor, if any.
   * @param unit        Unit of the sensor reading, for example Celsius, Pascal, Voltage, kWh
   * @param enabled     Is sensor enabled.
   */
  constructor(public id: string, public name: string, public hardwareId: string, public unit: Unit, public enabled: boolean) {
  }
}

// TODO: move SensorData class to common repository
export class SensorData {
  /**
   * Constructor.
   * @param id          Id of the sensor.
   * @param timestamp   Timestamp the sensor value was stored.
   * @param value       Numeric value of the sensor.
   */
  constructor(public id: string, public timestamp: number, public value: number) {}
}

// TODO: move to common repository
export const getStringValue = (property: string, item: AttributeMap): string => {
  if (isNullOrUndefined(item[property]) || isNullOrUndefined(item[property].S)) {
    throw new Error(`Missing value ${property}`);
  }

  return item[property].S as string;
};

// TODO: move to common repository
export const getNumberValue = (property: string, item: AttributeMap): number => {
  if (isNullOrUndefined(item[property]) || isNullOrUndefined(item[property].N)) {
    throw new Error(`Missing value ${property}`);
  }

  return parseFloat(item[property].N as string);
};

// TODO: move to common repository
export const getUnitValue =  (property: string, item: AttributeMap): Unit => {
  const stringValue = getStringValue(property, item);
  if (!Object
    .values(Unit)
    .includes(stringValue)) {
    throw new Error(`Configuration contains illegal unit value ${stringValue}`);
  }

  return stringValue as Unit;
};

// TODO: move to common repository
export const getBooleanValue =  (property: string, item: AttributeMap): boolean => {
  if (isNullOrUndefined(item[property]) || isNullOrUndefined(item[property].BOOL)) {
    throw new Error(`Missing value ${property}`);
  }

  return item[property].BOOL as boolean;
};
