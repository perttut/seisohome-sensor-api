// tslint:disable-next-line:no-implicit-dependencies
import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';
import { DynamoDB } from 'aws-sdk';
import { DeleteItemInput } from 'aws-sdk/clients/dynamodb';
import { isNullOrUndefined } from 'util';
import { lambdaReturnValue } from './util/lambda-util';
import { SENSOR_CONFIG_TABLE } from './util/sensor-common';

const deleteSensorConfig = async (id: string): Promise<void> => {
  const dynamoDb = new DynamoDB({apiVersion: '2012-08-10'});
  const createDeleteItem = (sensorConfigId: string): DeleteItemInput => ({
    TableName: SENSOR_CONFIG_TABLE,
    Key: {
      id: {S: sensorConfigId}
    }
  });
  await dynamoDb.deleteItem(createDeleteItem(id))
    .promise();
};

// tslint:disable-next-line:no-unused-variable
export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  if (isNullOrUndefined(event.pathParameters) || isNullOrUndefined(event.pathParameters.id)) {
    return lambdaReturnValue(500, 'Path parameter id missing');
  }
  const id = event.pathParameters.id;

  try {
    await deleteSensorConfig(id);

    return lambdaReturnValue(200, 'OK');
  } catch (error) {
    const errorMsg = `Unable to delete sensor configuration with id ${id} from DynamoDB`;
    console.error(errorMsg, error);

    return lambdaReturnValue(500, `${errorMsg}: ${error}`);
  }
};
