# seisohome-sensor-api

API for managing sensors.

## AWS deployment

### Deploy services

Note that if deployment complains about _InvalidParameterException: Domain already exists_ (it starts complaining after
initial deployment), you can remove the domain (Domain name) from AWS console and re-run deployment. Domain 
configuration could be replaced with AWS CLI, but then _serverless.yml_ would not be complete. Another option is to 
split _serverless.yml_ into pieces and deploy domain change only if needed. 

```bash
export AWS_DEFAULT_REGION=eu-west-1
export AWS_SECRET_ACCESS_KEY=...
export AWS_ACCESS_KEY_ID=...
npm install
npm run deploy
```

### Add AWS Cognito pool user

Use AWS console to create pool user, which will be allowed to access API Gateway REST interfaces.

### Test access

Initiate OAuth2 Authorization Code Grant using the following URL:
```
GET https://seiso-home-pool.auth.eu-west-1.amazoncognito.com/login?response_type=code&client_id=<client-id>&redirect_uri=<redirect-uri>
```

From redirect URL, find out code parameter.

Post the code parameter to Cognito:
```
POST https://seiso-home-pool.auth.eu-west-1.amazoncognito.com/oauth2/token
```

Set Content-Type header to application/x-www-form-urlencoded.

Pass body as _x-www-form-urlencoded_ with the following content.

| Key          | Value |
|--------------|-------|
| grant_type   | authorization_code |
| client_id    | [client-id] |
| redirect_uri | [redirect-uri] |
| code         | [code parameter from earlier GET] |
 

The response contains three tokens: id_token, access_token and refresh_token. Set id_token as is to Authorization header
and call one of the endpoints.
